package com.accenture.testing;
import org.junit.jupiter.api.*;

import com.accenture.entity.Product;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.persistence.*;
//add external Jar files Demo1
public class SelectTest 
{
	EntityManager em=null;
	EntityManagerFactory factory=null;

	
	@BeforeAll
	public void setup() 
	{
	factory=Persistence.createEntityManagerFactory("unit1");
	em =factory.createEntityManager();
	}
	@Test
	public void selectTest() 
	{
	Product p=em.find(Product.class, 102);
	assertEquals("NewlyAdded",p.getpName());
	assertEquals(346,p.getPrice());
	}
	@AfterAll
	public void close() 
	{
		em.close();
		factory.close();
	}
	

}
