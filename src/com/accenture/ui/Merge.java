package com.accenture.ui;

import javax.persistence.*;

import com.accenture.entity.Product;

public class Merge {
public static void main(String[] args) 
{
	//Talks to DB ,So line 9,10 is same for all
	EntityManagerFactory factory =Persistence.createEntityManagerFactory("unit1");//opens DB & loads Config
	EntityManager em =factory.createEntityManager();//performs rest DB Operations
	
	//Making changes in Db ,so begin&commit
	em.getTransaction().begin();
	
	Product p=new Product(102,"Newly Added",346,87);
	em.merge(p);
	
	em.getTransaction().commit();
	
	em.close();
	factory.close();
}
}
