package com.accenture.ui;

import javax.persistence.*;

import com.accenture.entity.Product;

public class Insert {
public static void main(String[] args) 
{
	//Talks to DB
	EntityManagerFactory factory =Persistence.createEntityManagerFactory("unit1");//opens DB & loads Config
	EntityManager em =factory.createEntityManager();//performs rest DB Operations
	
	//Insert Operation is done as follow (begin-commit)
	em.getTransaction().begin();//Transaction is req only when changing data in DB
	
	Product p=new Product(110,"RAMANA",6500,8);
	Product p1=new Product(120,"VANI",5500,7);
	
	// Creates row using above data
	em.persist(p); //similar to insert into product values(110,"RAMANA",6500,8);
	em.persist(p1); 
	
	em.getTransaction().commit();
	
	em.close();
	factory.close();
}
}
