package com.accenture.ui;

import javax.persistence.*;

import com.accenture.entity.Product;

public class Delete {
public static void main(String[] args) 
{
	//Talks to DB ,So line 9,10 is same for all
	EntityManagerFactory factory =Persistence.createEntityManagerFactory("unit1");//opens DB & loads Config
	EntityManager em =factory.createEntityManager();//performs rest DB Operations
	
	//Changing data in DB ,so begin&commit
	em.getTransaction().begin();
	
	Product temp=em.find(Product.class, 102);
	if(temp!=null) {em.remove(temp);}
	else {System.out.println("Data not available to delete");}
	
	em.getTransaction().commit();
	
	em.close();
	factory.close();
}
}
