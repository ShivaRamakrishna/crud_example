package com.accenture.ui;

import javax.persistence.*;

import com.accenture.entity.Product;

public class Update {
public static void main(String[] args) 
{
	//Talks to DB ,So line 9,10 is same for all
	EntityManagerFactory factory =Persistence.createEntityManagerFactory("unit1");//opens DB & loads Config
	EntityManager em =factory.createEntityManager();//performs rest DB Operations
	
	//Making changes in Db ,so begin&commit
	em.getTransaction().begin();
	
	Product temp=em.find(Product.class, 104);
	if(temp!=null) 
	{
		temp.setpName("Taduri");
		temp.setPrice(27);
	}
	else {System.out.println("Data not available to update");}
	
	em.getTransaction().commit();
	
	em.close();
	factory.close();
}
}
