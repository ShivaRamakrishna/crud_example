package com.accenture.ui;

import javax.persistence.*;

import com.accenture.entity.Product;

public class Select {
	public static void main(String[] args) 
	{
	new Select().selectMethod();
	}
	public void selectMethod() 
	{
		//Talks to DB ,So below 2 lines is same for all
			EntityManagerFactory factory =Persistence.createEntityManagerFactory("unit1");//opens DB & loads Config
			EntityManager em =factory.createEntityManager();//performs rest DB Operations
			
			Product temp=em.find(Product.class,120); //similar to select * from product where id='120'
			System.out.println(temp);
			
			em.close();
			factory.close();
	}
}
